﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryCalculator
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                args = new string[2];
                Console.WriteLine("Enter number one");
                args[0] = Console.ReadLine();
                Console.WriteLine("Enter number two");
                args[1] = Console.ReadLine();
            }

            // reverse both inputs for the calculation
            char[] chars1 = args[0].ToCharArray().Reverse().ToArray();
            char[] chars2 = args[1].ToCharArray().Reverse().ToArray();

            bool transfer = false;
            // initialize result list
            var result = new List<char>();

            // determine which array is longer and then take this length
            int length = chars1.Length >= chars2.Length ? chars1.Length : chars2.Length;

            // loop over each char by using the predefined length
            for (int i = 0; i < length; i++)
            {
                // checks if there is even a char in the current slot
                bool currentChar1 = (chars1.Length - 1) >= i && chars1[i] == '1';
                bool currentChar2 = (chars2.Length - 1) >= i && chars2[i] == '1';

                switch (currentChar1)
                {
                    case false when !currentChar2 && !transfer:
                        transfer = false;
                        result.Add('0');
                        break;
                    case true when !currentChar2 && !transfer:
                    case false when currentChar2 && !transfer:
                    case false when !currentChar2:
                        transfer = false;
                        result.Add('1');
                        break;
                    case true when currentChar2 && !transfer:
                    case true when !currentChar2:
                    case false:
                        transfer = true;
                        result.Add('0');
                        break;
                    default:
                        transfer = true;
                        result.Add('1');
                        break;
                }
            }

            //if there's a transfer add it to the result
            if (transfer)
            {
                result.Add('1');
            }

            //reverse back
            result.Reverse();
            // convert to string for outputting
            string output = string.Join("", result.ToArray());

            Console.WriteLine(output);
        }
    }
}